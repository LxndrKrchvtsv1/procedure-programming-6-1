public class Main {
    public static void main(String[] args) {
        int leastCommonMultiple = getLeastCommonMultiple(48, 60);
        print("Наименьшее общее кратное", leastCommonMultiple);

        int leastCommonMultipleOf3 = getLeastCommonMultipleOf3(3, 9, 6);
        print("Наименьшее общее кратное 3-х чисел", leastCommonMultipleOf3);

        int greatestCommonsDivisorOf4 = getGreatestCommonsDivisorOf4(24, 32, 48, 64);
        print("Наибольший общий делитель 4-х чисел", greatestCommonsDivisorOf4);

        int factorialSum = getSumOfFactorialsOddNums();
        print("Сумма факториалов нечетных чисал от 1 до 9", factorialSum);
    }

    static int getFactorialOfNumber(int n) {
        int factorial = 1;
        while (n > 0) {
            factorial = n * factorial;
            n--;
        }

        return factorial;
    }

    static int getSumOfFactorialsOddNums() {
        int n = 9;
        int factorialSum = 0;
        while (n >= 1) {
            if (n % 2 != 0) {
                factorialSum += getFactorialOfNumber(n);
            }
            n--;
        }

        return factorialSum;
    }

    static int getLeastCommonMultiple(int x, int y) {
        int greatestCommonDivisor = getGreatestCommonDivisor(x, y);

        return x * y / greatestCommonDivisor;

    }

    static int getLeastCommonMultipleOf3(int x, int y, int z) {
        int leastCommonMultiple = getLeastCommonMultiple(x, y);

        return getLeastCommonMultiple(leastCommonMultiple, z);
    }

    static int getGreatestCommonDivisor(int x, int y) {
        if (x % y == 0) {
            return Math.min(x, y);
        } else {
            while (x % y != 0) {
                int divisionReminder = x % y;
                x = y;
                y = divisionReminder;
            }
            return y;
        }
    }

    static int getGreatestCommonsDivisorOf4(int a, int b, int c, int d) {
        int ABDivisor = getGreatestCommonDivisor(a, b);
        int CDivisor = getGreatestCommonDivisor(c, ABDivisor);

        return getGreatestCommonDivisor(d, CDivisor);
    }

    static void print(String message, int number) {
        System.out.print(message + ": " + number + ";" + "\n");
    }
}
